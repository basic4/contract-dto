package com.starritt.contract.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ContractTest {

	@Test
	public void testName() {
		Contract contract = new Contract();
		contract.setName("NAME");
		assertEquals("NAME", contract.getName());
	}

	@Test
	public void testId() {
		Contract contract = new Contract();
		contract.setId(0);
		assertEquals(0, contract.getId());
	}
	
}
